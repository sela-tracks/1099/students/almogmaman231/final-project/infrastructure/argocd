
# Update kubeconfig for your EKS cluster
aws eks --region us-west-2 update-kubeconfig --name final-project-cluster

# Remove ArgoCD resources
kubectl delete -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

# Delete any remaining resources in the argocd namespace
kubectl delete all --all -n argocd

# Delete the argocd namespace
kubectl delete namespace argocd

# Remove the current context namespace setting
kubectl config set-context --current --namespace=default

echo "ArgoCD resources have been removed and the namespace has been deleted."

# Optionally, remove the EKS cluster from your kubeconfig
# Uncomment the following line if you want to remove the cluster configuration
# kubectl config delete-context $(kubectl config get-contexts -o name | grep final-project-cluster)