aws eks --region us-west-2 update-kubeconfig --name final-project-cluster
kubectl create namespace argocd
kubectl config set-context --current --namespace=argocd

kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
sleep 30

kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'


kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d

kubectl get svc #to find the load balancer service for connecting to publicy.

